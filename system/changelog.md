
## 0.0.4 - 2021-11-25
  * Fixed installation of node in command install-node
  * Update README and documentation.
  * Fixed bugs in scripts and documentation.

## 0.0.3 - 2021-08-23
  * Small updates to scripts.

## 0.0.2 - 2021-01-02

  * Created more alternatives for first setup.
  * Shortcut and script for locking computer.
  * Keyboard shortcuts.
  * Renamed and fixed some scripts.
  * Install emojis font.
  * Fixed bugs with wallpaper

## 0.0.1 - 2020-12-12

  * First version.
