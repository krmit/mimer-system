# Välkommen till Mimer system

Mimer system är en samling av små script för att skapa ett
så bra arbetsflöde som möjligt för utveckling i våra kurser. I 
första hand för node och typesscript eller html och css. Likaså är 
det tänkt att systemet de körs på ska vara ett Raspberry Pi OS som körs 
på en Raspberry PI 4B med 8GB.

Andra Linux OS som är debian baserade kan också delvis fungera, men 
andra varianter av Linux eller BSD samt Windows kommer inte fungera.

## Installation

Se till att du har startat Raspberry Pi, samt ställt in tangentbordet
och nätverket. Du ska inte ha bytt namn på din användare utan ha
default användaren pi.
 
  1. Börja med att öppna en terminal och gå till din hemmapp.
```sh
cd
```

 2. Laddad ned scripten från hemsidan:
```sh
curl -LOk https://htsit.se/f/s.zip
```

3. Packa upp filen i din hemmapp.
```sh
unzip s.zip
```

4. Du ska nu köra setup scriptet. 
**OBS! Detta kan ta mycket tid!**
```sh
./system/s
```

Starta sedan om datorn.

# Updatera

Vill du sedan uppdatera till senaste versionen av scripten, så ge bara
kommandot:

```sh
update
```
