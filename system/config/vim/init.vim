" My vimrc config

" # Initiate and install
" Install pluginmanager if not allredy done
" From https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation by junegunn
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Create dir
" From http://stackoverflow.com/questions/9987887/vimrc-when-backupdir-is-set-every-save-prompts-press-enter-to-continue
if !isdirectory(expand("~/.vim/backupdir/"))
        silent !echo "Creating backup dir..."
        silent !mkdir -p ~/.vim/backup
endif

if !isdirectory(expand("~/.vim/swap/"))
        silent !echo "Creating swap dir..."
        silent !mkdir -p ~/.vim/swap
endif

if !isdirectory(expand("~/.vim/undo/"))
        silent !echo "Creating undo dir..."
        silent !mkdir -p ~/.vim/undo
endif

" Install plugin
call plug#begin()
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'pangloss/vim-javascript'    " JavaScript support
    Plug 'leafgarland/typescript-vim' " TypeScript syntax
    Plug 'neoclide/coc.nvim', {'branch': 'release'} " 
call plug#end()
":PlugInstall

" # Config
" For all
"
set nocompatible

" Colors

syntax enable           " enable syntax processing

" Show Code

set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
set showmatch           " highlight matching [{()}]

" Menus

set wildmenu            " visual autocomplete for command menu

" Tabs

set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces

" Shortcuts

inoremap jk <esc>   " jk is escap
nnoremap <C-q> :q<CR>
nnoremap <C-s> :w<CR>

" From http://stackoverflow.com/questions/327411/how-do-you-prefer-to-switch-between-buffers-in-vim
" Tab triggers buffer-name auto-completion

set wildchar=<Tab> wildmenu wildmode=full
let mapleader = "f"

map <Leader>p :bprev<Return>
map <Leader>n :bnext<Return>
map <Leader>d :bd<Return>
map <Leader>l :ls<Return>
map <Leader>f :b#<Return>

set hidden

" For swedish layoute

:nmap ö :
:nmap ä $

" Backups

set backupdir^=~/.vim/backup//              " where to put backup files
set directory^=~/.vim/swap//                " where to put swap files
set undodir^=~/.vim/undo//

set backup
set writebackup

" # Plugin configuration
" ## COC
" From https://breuer.dev/blog/top-neovim-plugins by Felix Breuer

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

inoremap <silent><expr> <C-space> coc#refresh()

"GoTo code navigation
nmap <leader>g <C-o>
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gt <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nmap <leader>rn <Plug>(coc-rename)

"show all diagnostics.
nnoremap <silent> <space>d :<C-u>CocList diagnostics<cr>
"manage extensions.
nnoremap <silent> <space>e :<C-u>CocList extensions<cr>

":CocInstall coc-json coc-tsserver
