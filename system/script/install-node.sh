#!/usr/bin/env bash

NODE_DIR=$APP_DIR/node;

echo "Install node version $NODE_VERSION/"
FILE=node-v$NODE_VERSION-linux-armv7l.tar.xz
DIR=./node-v$NODE_VERSION-linux-armv7l
rm -rf $NODE_DIR;
curl -L https://nodejs.org/dist/v$NODE_VERSION/$FILE -o $APP_DIR/node.tar.gz
tar -xf $HOME/app/node.tar.gz
mv $DIR $NODE_DIR; 
rm $APP_DIR/node.tar.gz

echo "Configurate PATH"
touch $MIMER_CONFIG/bashrc;
echo -e '\nexport PATH=$PRIMARY/app/node/bin:$PATH;' >> $MIMER_CONFIG/bashrc;
echo "You need to reboot to use the new version of node."


