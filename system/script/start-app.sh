#!/usr/bin/env bash

# This script should be startet first.

# Terminal
wid-title "LXTerminal" echo -n ""  > $WID_DIR/terminal;

# Chatt
set-foucus-by-title chatt "SchoolSoft - Mozilla Firefox" firefox -p chatt;

# Web
set-foucus-by-title web "Google Drive - Mozilla Firefox" firefox -p web;
