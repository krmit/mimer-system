#!/usr/bin/env bash

for f in $SYSTEM/script/*; do
    baseName=`basename $f | cut -d "." -f 1`;
    cp $f $APP_DIR/bin/$baseName;
done
