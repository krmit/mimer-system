#!/usr/bin/env bash

NODE_DIR=$APP_DIR/node;

echo "OBS! Need to have give the command install-node first."
echo "Or install a user copy of node by yourself."

echo "Install ncu to update node package"
npm install -g npm-check-updates

echo "Install to convert md to html"
npm install -g ghmd

echo "Install ncu to convert js-yaml"
npm install -g js-yaml
