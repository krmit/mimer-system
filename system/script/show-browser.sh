#!/usr/bin/env bash

# Show live page
firefox -p dev --new-instance http://127.0.0.1:8080/index.html &
sleep 5;
TITLE="Mozilla Firefox";
while [ "$WID" == "" ]; do
  WID=`wmctrl -l | grep "$TITLE" | cut -d" " -f 1 | tail -n 1`;
done
echo $WID > $WID_DIR/f4;
