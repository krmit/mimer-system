#!/usr/bin/env bash


# For IDE
set-foucus-by-title f1 "VSCodium" code ./;

# For doing test and run priograms
set-foucus-by-title f2 "Run and test" open-terminal-run;

# For handling files
set-foucus-by-title f3 `basename $PWD` pcmanfm ./;

# For watch script
set-foucus-by-title f6 "Watch script" open-terminal-watch;

