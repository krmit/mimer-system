#!/usr/bin/env bash
WID_DIR=/home/pi/.primary/wid
FILE=$WID_DIR/$1;
echo $FILE >> /home/pi/.config/mimer/debug.log;

WID=`[ -r $FILE ] && cat $FILE`;
echo $WID >> /home/pi/.config/mimer/debug.log;

if [ ! "$WID" == "" ];
then 
  wmctrl -a $WID -i;
else
  echo "No such program is open."
fi
