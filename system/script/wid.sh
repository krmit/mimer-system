#!/usr/bin/env bash

# From https://unix.stackexchange.com/a/51817 by Bobby

$1 &
PID=$!
while [ "$WID" == "" ]; do
        WID=$(wmctrl -lp | grep $PID | cut "-d " -f1)
done

echo $WID
