#!/usr/bin/env bash

echo "Install vscode"
curl -L https://github.com/VSCodium/vscodium/releases/download/$VSCODIUM_VERSION/VSCodium-linux-armhf-$VSCODIUM_VERSION.tar.gz -o $APP_DIR/vscodium.tar.gz;
rm -rf $APP_DIR/vscodium;
mkdir $APP_DIR/vscodium;
tar xf $APP_DIR/vscodium.tar.gz -C $APP_DIR/vscodium/;
rm $APP_DIR/vscodium.tar.gz;
ln -sf $APP_DIR/vscodium/bin/codium $APP_DIR/bin/code;
