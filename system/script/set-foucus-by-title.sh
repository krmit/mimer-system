#!/usr/bin/env bash

# Title
FILE=$1;
shift;
TITLE=$1;
shift;

# Terminal
$@ &
while [ "$WID" == "" ]; do
  WID=`wmctrl -l | grep "$TITLE" | cut -d" " -f 1 | head -n 1`;
done
echo $WID > $WID_DIR/$FILE;
