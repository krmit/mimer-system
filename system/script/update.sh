#!/usr/bin/env bash

# Download file and unzip
curl -Lk https://htsit.se/f/s.zip -o $HOME/s.zip;
unzip -o $HOME/s.zip;
rm $HOME/s.zip;
$HOME/system/tools/setup.sh;
