#!/usr/bin/env bash

# Title
TITLE=$1;
shift;

# Terminal
$@ &
while [ "$WID" == "" ]; do
  WID=`wmctrl -l | grep "$TITLE" | cut -d" " -f 1 | head -n 1`;
done
echo $WID;
