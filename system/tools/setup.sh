#!/usr/bin/env bash

echo "Update arkive"
sudo apt update
sudo apt upgrade -y

echo "Install usefull programs"
sudo apt install cowsay -y
sudo apt install vim -y
sudo apt install profile-sync-daemon -y
# sudo apt install conky -y
sudo apt install figlet -y
sudo apt install wmctrl -y
sudo apt install xclip -y

echo "Install emojis font."
sudo apt install fonts-noto-color-emoji -y

echo "Config"
setxkbmap se

echo "Read config"
source $HOME/system/config.conf;

echo "Get today info"
source $HOME/system/tools/getDateInfo.sh;

echo "Create importen maps"
mkdir -p $APP_DIR;
mkdir -p $APP_DIR/bin;
mkdir -p $DEV_DIR;
mkdir -p $MIMER_CONFIG;

echo "Link config"
ln -sf $SYSTEM/config/* $CONFIG/ 2> /dev/null;
ln -sf $SYSTEM/file/bashrc $HOME/.bashrc 2> /dev/null;

# From https://www.novaspirit.com/2017/02/23/desktop-widget-raspberry-pi-using-conky/
#ln -sf $SYSTEM/file/conkyrc $HOME/.conkyrc 2> /dev/null;
#sudo ln -sf $SYSTEM/file/conky.sh /usr/bin/conky.sh 2> /dev/null;
#sudo ln -sf $SYSTEM/file/conky.desktop /etc/xdg/autostart/ 2> /dev/null;

echo "This chnages could cause system to not boot"
echo "Startup script"
sudo cp -f $SYSTEM/file/rc.local /etc/rc.local 2> /dev/null;

echo "Boot script"
# Not working
#sudo cp -f $SYSTEM/file/config.txt /boot/config.txt 2> /dev/null;

echo "Set wallpaper"
pcmanfm --set-wallpaper $SYSTEM/image/$WALLPAPER;
echo "End of danger"

echo "Create config for chromium cache"
echo "--disk-cache-dir=\"$CHROMIUM_CACHE\"" > $CONFIG/chromium-flags.conf;

echo "Create a ram disk"
grep -q 'For primary map' /etc/fstab || 
sudo sh -c "echo \"# For primary map\ntmpfs   $PRIMARY         tmpfs   rw,nodev,nosuid,size=1G          0  0\n\" >> /etc/fstab";

#echo "Inset cron jobs to cron"
# /usr/bin/crontab $HOME/system/file/crontab

echo "Copy script to app/bin"
# From https://stackoverflow.com/a/28239297
for f in $SYSTEM/script/*; do
    baseName=`basename $f | cut -d "." -f 1`;
    cp $f $APP_DIR/bin/$baseName;
done

echo "Start psd demon"
systemctl --user start psd;

echo "Log setup"
VERSION=`cat $HOME/system/version`;
LOG_DATE=`date +"%Y-%m-%d %H:%M:%S"`;
echo "[SETUP] $LOG_DATE Version: $VERSION" >> $MIMER_LOG;
echo "You should REBOOT yout computer!";
