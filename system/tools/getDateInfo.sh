TODAY=`date +%j` 
SCHOOL_START=`date -d "20210816" +%j`; 
SCHOOL_END=`date -d "20210827" +%j`;
JUHL_START=`date -d "20211224" +%j`; 
JUHL_END=`date -d "20211226" +%j`; 
LUCIA=`date -d "20211213" +%j`; 
ADVENT_START=`date -d "20211201" +%j`; 
ADVENT_END=`date -d "20211223" +%j`;
NEW_YEAR_START=`date -d "20211231" +%j`; 
NEW_YEAR_END=`date -d "20220101" +%j`; 
SNOW_START=`date -d "20211227" +%j`; 
SNOW_END=`date -d "20220106" +%j`; 


if [ "$TODAY" -eq "$LUCIA" ];
then
  TODAY_MSG="Ha en stämningsfull Lucia";
  TODAY_TYPE="lucia";
  WALLPAPER="lucia.jpg";
elif [ "$TODAY" -ge "$JUHL_START" ] && [ "$JUHL_END" -ge "$TODAY" ];
then
  TODAY_MSG="God Jul";
  TODAY_TYPE="juhl";
  WALLPAPER="xmas.jpg";
elif [ "$TODAY" -ge "$ADVENT_START" ] && [ "$ADVENT_END" -ge "$TODAY" ];
then
  TODAY_MSG="Trevlig Advent";
  TODAY_TYPE="advent";
  WALLPAPER="advent.jpg";
elif [ "$TODAY" -ge "$NEW_YEAR_START" ] || [ "$NEW_YEAR_END" -ge "$TODAY" ];
then
  TODAY_MSG="Gott Nytt År";
  TODAY_TYPE="new-year";
  WALLPAPER="new-year.jpg";
elif [ "$TODAY" -ge "$SNOW_START" ] || [ "$SNOW_END" -ge "$TODAY" ];
then
  TODAY_MSG="God Fortsättning";
  TODAY_TYPE="snow";
  WALLPAPER="snow.jpg";
elif [ "$TODAY" -ge "$SCHOOL_START" ] || [ "$SCHOOL_END" -ge "$TODAY" ];
then
  TODAY_MSG="Valkommen till HTS!";
  TODAY_TYPE="school";
  WALLPAPER="school.jpg";
else
  TODAY_MSG="God Dag";
  TODAY_TYPE="normal";
  WALLPAPER="starstabel.jpg";
fi
