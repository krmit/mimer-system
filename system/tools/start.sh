#!/usr/bin/env bash

echo "Read config"
source $HOME/system/config.conf;

echo "Create importen maps in primary"
mkdir -p $CHROMIUM_CACHE;
mkdir -p $WID_DIR;

echo "Copy app to primary!"
cp -r $APP_DIR $APP_DIR_PRIMARY;

echo "Register inlog"
LOG_DATE=`date +"%Y-%m-%d %H:%M:%S"`;
echo "[START] $LOG_DATE User: $USER" >> $MIMER_LOG;

echo "Get today info"
echo "Copy wallpaper"
sleep 180 && source $HOME/system/tools/getDateInfo.sh && ln -sf $SYSTEM/image/$WALLPAPER $SYSTEM/image/wallpaper.jpg 2> /dev/null && pcmanfm --set-wallpaper $SYSTEM/image/$WALLPAPER;
