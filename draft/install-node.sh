#!/usr/bin/env bash

echo "Install node"
# This will not work bicause this node version is for android. 
curl https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION.tar.gz -o $APP_DIR/node.tar.gz ;
tar xzvf $APP_DIR/node.tar.gz -C $APP_DIR/
rm node.tar.gz;
rm -rf node;
mv node-v$NODE_VERSION node;
ln -s $APP_DIR/node/bin/* $APP_DIR/bin/

