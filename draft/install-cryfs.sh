#!/usr/bin/env bash

echo "Install cryfs"
export CRYFS-VERSION;
sudo apt install git g++ cmake make libcurl4-openssl-dev libssl-dev libfuse-dev python python3-pip;
sudo pip3 install conan;
cd $HOME/app/;
curl -LO "https://github.com/cryfs/cryfs/archive/$CRYFS_VERSION.zip";
unzip "$CRYFS_VERSION.zip";
cd cryfs-$CRYFS_VERSION;
mkdir cmake && cd cmake;
cmake .. -DCMAKE_BUILD_TYPE="Release";
make;
sudo make install;
rm -rf cryfs-$CRYFS_VERSION;

